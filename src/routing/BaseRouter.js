import {useHistory, useRouteMatch} from "react-router-dom";
import React, {lazy, Suspense, useEffect} from "react";
import queryString from "query-string";
import LoadingPage from "../component/LoadingPage/LoadingPage";

import {withCommonPropsLite} from "../component/interfacehook/InterfaceHook";
import SnackbarRender from "../component/Snackbar/SnackbarRender";
import DefaultLoading from "../component/LoadingPage/DefaultLoading";
import Sample from "../screen/Sample/Sample";

const SmallDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/SmallDialogDrawerHandler"));
const SignIn = lazy(() => import("../screen/Sample/Sample"));


export const BaseRouter = (props) => {
    let history = useHistory();
    let match = useRouteMatch();
    let {store} = props
    let {appLoading, loading, enabledView} = store

    let searchValues = (search) => {
        return queryString.parse(search)
    }
    if (!window.TNDhistory) {
        window.TNDhistory = history
    }
    if (!window.TNDmatch) {
        window.TNDmatch = match
    }
    if (!window.TNDlocation) {
        window.TNDlocation = history.location
    }
    if (!window.TNDsearchValues) {
        window.TNDsearchValues = searchValues
    }


    useEffect(() => {
        (async () => {
        })()

        appLoading.set(false)
        loading.set(false)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    if (appLoading.get) {
        return <LoadingPage/>
    }

    const renderContent = () => {
        switch (enabledView.get) {
            default:
                return <Sample/>
        }
    }

    return (<>
            <Suspense
                fallback={<LoadingPage/>}
            >
                <SnackbarRender/>
                <SmallDialogDrawerHandler/>
                {<DefaultLoading/>}
                {renderContent()}
            </Suspense>
        </>
    )
}

export default withCommonPropsLite(BaseRouter)


