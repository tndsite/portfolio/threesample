import React from 'react';
import {createRoot} from 'react-dom/client';
import './index.css';
import ThreeSample from "./ThreeSample";
import {AppProvider} from "./component/MobxContext/AppContext";
import './asset/elucidCircularA/index.css';

const container = document.getElementById('root');
const root = createRoot(container);

global = {
    ...global,
    trl: {
    }
}
root.render(<AppProvider><ThreeSample/></AppProvider>);

