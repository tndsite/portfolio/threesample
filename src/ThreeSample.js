import {CssBaseline, ThemeProvider} from "@mui/material";
import React, {useEffect, useMemo} from "react";
import BaseRouter from "./routing/BaseRouter";
import {useAppStore} from "./component/MobxContext/AppContext";
import {observer, Observer} from "mobx-react";
import {BrowserRouter} from "react-router-dom";
import CacheBuster from "./CacheBuster";
import packageJson from "../package.json"
import ErrorBoundary from "./ErrorBoundary";
import NetworkDetector from "./NetworkDetecter";
import {IntlProvider} from "react-intl";
import {commonHandledRequest,} from "./constants/Utility";
import moment from "moment";
import BaseAxios from "./constants/BaseAxios";
import {NO_LOADING} from "./constants/Constants";


global.appVersion = packageJson.version

if (process.env.REACT_APP_NODE_ENV === "production") {
    console.info = () => {
    };
    console.debug = () => {
    };
    console.log = () => {
    };
    console.group = () => {
    };
    console.groupEnd = () => {
    };
    console.track = {
        trackEvent: () => {
        }
    };
}


let tran = {}
const ThreeSample = () => {
    let store = useAppStore();
    let {language, theme,localLoading} = store


    useMemo(() => {
        tran = JSON.parse(localStorage.getItem("newTrans")) || {}
    }, [])
    useEffect(() => {
        (async () => {

            window.TNDglobalStoreMobx = store
            localLoading.set(false)
            await getLocales()
            for (let locale of language.locales) {

                await commonHandledRequest({
                    action: async () => {
                        let res = await BaseAxios.get('/service/translations-tool/translation/check-last-edit/' + locale.id,);
                        let updatedDate =res.data.updatedAt
                        let savedValue= localStorage.getItem("latestUpdateTranslation" + locale.id)
                        if (
                            !savedValue
                            ||
                            (savedValue && savedValue==="undefined")
                            ||
                            moment(savedValue).isBefore(updatedDate)
                        ) {
                            await getTranslations(locale.id)
                            localStorage.setItem("latestUpdateTranslation" + locale.id,updatedDate)
                        }
                    },
                    cbOnError: () => {
                    },
                    loadingType: NO_LOADING,
                    translationKeyError: "checkTranslation",
                });

            }

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getLocales = async () => {
        await commonHandledRequest({
            action: async () => {
                let res = await BaseAxios.get('/service/translations-tool/locale/read/',);
                language.setLocales(res.data)
            },
            cbOnError: () => {
            },
            loadingType: NO_LOADING,
            translationKeyError: "readLocale",
        });
    }


    const getTranslations = async (id="") => {
        await commonHandledRequest({
            action: async () => {
                let res = await BaseAxios.get('/service/translations-tool/read-all/'+id,);
                language.setList(res.data)
            },
            cbOnError: () => {
            },
            loadingType: NO_LOADING,
            translationKeyError: "readTranslation",
        });
    }

    const postTerm = async (data) => {
        await commonHandledRequest({
            action: async () => {
                await BaseAxios.post('/service/translations-tool/term/create', data);

            },
            loadingType: NO_LOADING,
            cbOnError: () => {
            },
            translationKeyError: "postTerm",
        });
    }
    return (<CacheBuster>
            <ThemeProvider theme={theme.get} key={theme.get}>
                <CssBaseline/>
                <ErrorBoundary>
                    <IntlProvider
                        key={language.get}
                        messages={{...language.list[language.get]}}
                        locale={language.get}
                        onError={async (err) => {
                            let missingTerm = String(err).split('"')[1]
                            console.debug("Missing translation for key: " + missingTerm + " for locale: " + language.get,tran)
                            try {
                                if (!Object.keys(tran).some(x => x === missingTerm)) {
                                    await postTerm({
                                        key: String(err).split('"')[1]
                                    })
                                    tran[String(err).split('"')[1]] = ""

                                    localStorage.setItem("newTrans", JSON.stringify(tran))

                                }
                            } catch (e) {
                                console.error("Error generic:", e, String(err).split('"')[1])
                            }
                        }}
                    >
                            <NetworkDetector>
                                <Observer>{() => (
                                    <>
                                        <BrowserRouter basename={process.env.PUBLIC_URL}>
                                            <BaseRouter
                                                store={store}
                                            />
                                        </BrowserRouter>
                                    </>
                                )}
                                </Observer>
                            </NetworkDetector>
                    </IntlProvider>
                </ErrorBoundary>
            </ThemeProvider>
        </CacheBuster>
    );

}


export default observer(ThreeSample);

