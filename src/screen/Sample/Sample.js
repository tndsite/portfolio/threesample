import {Container, Stack, Typography,} from "@mui/material";
import React, {useEffect} from "react";
import {withCommonPropsLite} from "../../component/interfacehook/InterfaceHook";
import {FormattedMessage} from "react-intl";
import packageJson from "../../../package.json";
import BG from "../../asset/image/beige-paper2.png";
import ModelTest from "../ModelTest";
import CubeTest from "../CubeTest";

const Sample = (props) => {
    let {
        store, intl, history, isMobile
    } = props
    let {enabledView, loading, theme} = store

    let isDark = theme.style === "dark"
    useEffect(() => {
        (async () => {
            loading.set(false)
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (<Container
            id={"main-content-scrollable"}
            maxWidth="false"
            sx={{
                height: "100%",
                backgroundColor: "background.default",
                overflow: "auto", paddingTop: "0px!important",


            }}
        >
            <Stack
                id={"bg-pattern"}
                sx={{
                    height: "100%",
                    width: "100%",
                    backgroundColor: "background.default",
                    overflow: "auto", paddingTop: "0px!important",
                    backgroundImage: "url(" + BG + ")",
                    backgroundSize: "auto",
                    backgroundRepeat: "repeat",
                    // backgroundRepeat: "no-repeat",
                    opacity: 0.3,
                    position: "absolute",
                    zIndex: "0"

                }}
            />
            <Stack
                id={"main-content-sign-in"}
                width={"100%"}
                padding={"20px"}
                sx={{
                    height: "100%",
                    // backgroundColor: "background.default",
                    // overflowY: "auto",
                    overflow: "hidden",
                    alignItems: "center",
                    zIndex: 1,
                    position: "relative",
                    paddingRight:"310px"
                }}
                gap={"20px"}
                justifyContent={"center"}
            >

                {/*<Stack>*/}
                {/*    <Typography variant={"title30"}>*/}
                {/*        <FormattedMessage id={"three.backoffice"}/>*/}
                {/*    </Typography>*/}
                {/*</Stack>*/}
                <Stack width={isMobile ? "100%" : "80%"} maxWidth={"1400px"} alignItems={"flex-start"} gap={"20px"}
                       sx={{position: "relative",
                           height: "-webkit-fill-available",
                }}>
                    <ModelTest/>
                </Stack>
            </Stack>
        </Container>
    )


}

export default withCommonPropsLite(Sample)

