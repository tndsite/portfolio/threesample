import React, { useEffect, useRef } from "react";
import * as THREE from "three";
import {Stack} from "@mui/material";
import {GLTFLoader} from "three/addons/loaders/GLTFLoader";
import soldierModel from "../asset/model/Soldier.glb";

const CubeTest = () => {
    const refContainer = useRef(null);

    useEffect(() => {
        // Create a scene
        const scene = new THREE.Scene();

        // Create a camera
        const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        camera.position.set(0, 0, 5); // Center the camera

        // Create a geometry
        const geometry = new THREE.BoxGeometry(1, 1, 1);

        // Create a material
        const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });

        // Create a mesh (the cube)
        const cube = new THREE.Mesh(geometry, material);

        // Add the cube to the scene
        scene.add(cube);
        let model, skeleton, mixer, clock;

        const loader = new GLTFLoader();
        loader.load(soldierModel, function (gltf) {
            model = gltf.scene;
            scene.add(model);
            model.traverse(function (object) {
                if (object.isMesh) object.castShadow = true;
            });

            //

            skeleton = new THREE.SkeletonHelper(model);
            skeleton.visible = false;
            scene.add(skeleton);

        });
        // Create a renderer with transparent background
        const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true }); // Set alpha to true

        // Set the size of the renderer
        renderer.setSize(window.innerWidth, window.innerHeight);

        // Add the renderer to the component
        refContainer.current.appendChild(renderer.domElement);

        // Create a render or animate function
        const animate = function () {
            requestAnimationFrame(animate);

            // Rotate the cube
            cube.rotation.x += 0.01;
            cube.rotation.y += 0.01;

            // Render the scene with the camera
            renderer.render(scene, camera);
        };

        // Call the animate function
        animate();
    }, []);

    return <Stack style={{ width: "100%", height: "100%" }} ref={refContainer} />;
};

export default CubeTest;
