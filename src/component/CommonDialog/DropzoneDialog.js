import React, {useCallback, useState} from 'react';
import PaperDraggable from "./PaperDraggable";
import {Button, Chip, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Paper, Stack} from "@mui/material";
import style from "./CommonDialog.module.css";
import {useDropzone} from "react-dropzone";
import {UploadFile} from "@mui/icons-material";
import {blueGrey} from "@mui/material/colors";

const color = blueGrey[100];

const DropzoneDialog = (props) => {
    let {
        open,
        onClose,
        cancelButtonText,
        submitButtonText,
        dialogTitle,
        dropzoneText,
        acceptedFiles,
        previewText,
        filesLimit,
        onSave
    } = props;

    let [files, setFiles] = useState([]);
    let onDrop = useCallback(acceptedFiles => {
        setFiles(acceptedFiles)
    })
    let {getRootProps, getInputProps, isDragActive} = useDropzone({
        onDrop,
        accept: acceptedFiles.join(","),
        maxFiles: filesLimit
    })


    let dialog = <Dialog
        id={"mainContainerCommonDialog"}
        open={open}
        onClose={onClose}
        PaperComponent={PaperDraggable}
        scroll='paper'
        aria-labelledby="draggable-dialog-title"
        className={style.dialogSafeArea}>
        <DialogTitle
            id="draggable-dialog-title"
        >
            {dialogTitle}
        </DialogTitle>
        <DialogContent
            style={{
                paddingTop: "20px",
                paddingBottom: "20px",
                display: "flex",
                flexDirection: "column"
            }}
        >
            <Paper sx={{flex: 1, padding: "20px", backgroundColor: isDragActive ? color : undefined}}
                   variant="elevation"
                   elevarion={36}
                   {...getRootProps()}>
                <Stack
                    height={"100%"}
                    alignItems={"center"}
                    justifyContent={"center"}>
                    <input {...getInputProps()} />
                    <Label font={"--text-font-h4"}>
                        {dropzoneText}
                    </Label>
                    <UploadFile fontSize={"large"}/>
                </Stack>
            </Paper>
            {files && files.length > 0 && <>
                <Typography variant={"h6"}>
                    {previewText}
                </Typography>
                <Grid container spacing={2}>
                    {files.map(f => {
                        console.debug("Files: ", f)
                        return <Grid key={f.name} item>
                            <Chip label={f.name} variant="outlined" onDelete={() => {
                                setFiles(files.filter((x) => {
                                    return x.name !== f.name
                                }))
                            }}/>
                        </Grid>
                    })}
                </Grid>
            </>}
        </DialogContent>
        <DialogActions>
            <Button
                variant={"text"}
                onClick={onClose}
            >
                {cancelButtonText}
            </Button>
            <Button
                disabled={files.length == 0}
                variant={"contained"}
                color={"primary"}
                onClick={() => {
                    onSave(files);
                    setFiles([])
                }}
            >
                {submitButtonText}
            </Button>
        </DialogActions>
    </Dialog>

    return dialog
};

export default DropzoneDialog;
