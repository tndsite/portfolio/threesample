import React, {Component, useState} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Drawer,
    IconButton,
    useMediaQuery
} from "@mui/material";
import PaperFullScreen from "./PaperFullScreen";
import style from "./CommonDialog.module.css"
import Paper from "@mui/material/Paper";
import {Close} from "@mui/icons-material";

class PaperDraggableBig extends Component {
    render() {
        return ( <Paper
                {...this.props}
                style={{
                    overflow: "hidden",
                    minHeight: "439px",
                    minWidth: "80vw"
                }}
            />
        );
    }
}


const CommonDialog = (props) => {

    let {
        open,
        handleClose,
        title,
        customContent,
        customActions,
        paperSizeIsSmall,
        paperSizeIsSmallest,
        paperSizeIsBig,
        paperSizeIsFullScreen,
        leftButtonText,
        rightButtonText,
        leftButtonOnClick,
        rightButtonOnClick,
        leftButtonVariant,
        leftStartIcon,
        rightButtonColor,
        rightButtonVariant,
        rightStartIcon,
        handleSubmit,
        content,
        customStyle,
        customKey,
        id,
        actionsFixedPosition,
        dontCloseOnBackdropClick,
        noScrollingExternal
    } = props;
    let isMobile = useMediaQuery('(max-width:868px)');
    const renderDefaultActions = () => {

        return <DialogActions
            sx={actionsFixedPosition?{
                position:"fixed",bottom:"10px",right:"10px",padding:0
            }:{
                flexDirection:isMobile?"column-reverse":undefined,
                padding: isMobile?"20px": undefined,

            }}
        >
            <Button size={"medium"}
                    color={rightButtonColor || "primary"}
                    type="submit"
                    id={customKey+"-right"}
                    key={customKey+"-right"}
                    variant={rightButtonVariant || "contained"}
                    icon={rightStartIcon ? rightStartIcon : undefined}
                    sx={{
                        fontSize: "14px",
                        height: "40px",
                        minWidth: isMobile?"100%":"133px",
                        borderRadius: "40px",
                        fontWeight: 600,
                        ...customStyle?.rightButton
                    }}
                    onClick={rightButtonOnClick ? (e) => {
                        e.preventDefault()
                        rightButtonOnClick(e)
                    } : undefined}

            >
                {rightButtonText || ""}
            </Button>

        </DialogActions>;
    };

    const renderDefaultContent = () => {

        return <form
            className={style.form}
            onSubmit={e => {
                e.preventDefault()
                if (!rightButtonOnClick) {
                    handleSubmit(e);
                }
            }}
        >
            <DialogContent

                sx={{
                    fontWeight:100,
                    maxWidth: "50vw",
                    padding: isMobile?"20px": undefined,
                    ...customStyle?.content

                }}
            >
                {content}
            </DialogContent>
            {customActions ?
                <DialogActions
                    sx={actionsFixedPosition?{
                        position:"fixed",bottom:"10px",right:"10px",padding:0
                    }:{
                        padding: isMobile?"20px": undefined,
                        flexDirection:isMobile?"column-reverse":undefined,
                    }}
                >
                    {customActions}
                </DialogActions> :
                renderDefaultActions()
            }
        </form>
    };


    let paper=PaperFullScreen;


    const renderDefault = () => {

        return <><DialogTitle
            sx={{
                fontWeight:600,
                fontSize:"20px",
                padding: isMobile?"20px": undefined,
                display:"flex",
                flexDirection:"row",
                justifyContent:"space-between",
                alignItems:"center",
                gap:"10px",
                ...customStyle?.title
            }}
            id="draggable-dialog-title"
        >

            {title}
            <IconButton
                onClick={handleClose}
                // sx={{position: "absolute", top: "20px", right: "20px"}}
            >
                <Close/>
            </IconButton>
        </DialogTitle>
            {customContent ?
                <DialogContent
                    sx={{
                        fontWeight:100,
                        padding: isMobile?"20px": undefined,
                        ...customStyle?.content
                    }}
                >
                    {customContent}
                </DialogContent>
                : renderDefaultContent()
            }
            {customContent && customActions ?
                <DialogActions
                    sx={actionsFixedPosition?{
                        position:"fixed",bottom:"10px",right:"10px",padding:0
                    }:{  padding: isMobile?"20px": undefined,
                        flexDirection:isMobile?"column-reverse":undefined,
                    }}
                >
                    {customActions}
                </DialogActions>
                : customContent &&
                renderDefaultActions()
            }</>
    }


    return <Dialog
        open={open}
        onClose={dontCloseOnBackdropClick?()=>{}:handleClose}
        PaperComponent={paper}
        scroll='paper'
        aria-labelledby="draggable-dialog-title"
        id={"mainContainerCommonDialog"}
        maxWidth={customStyle && customStyle.maxWidth ? customStyle.maxWidth : undefined}
        fullWidth={customStyle && customStyle.fullWidth}
        fullScreen={customStyle && customStyle.fullScreen}
        className={style.dialogSafeArea}
        PaperProps={{
            id,
            sx: {
                width: "100%",
                background: "transparent",
                backgroundColor:"background.paper",
                overflow: "hidden",
                height: "unset",
                maxHeight: "unset",
                ...customStyle?.paper
            }
        }}
        sx={{
            "& 	.MuiDialog-container": {
                overflow: noScrollingExternal?"hidden":"auto!important",
                padding: noScrollingExternal?undefined:"100px 0!important",
                display: "flex!important",
                alignItems: noScrollingExternal?"center":"flex-start!important",
                justifyContent: "center!important"
            }
        }}
    >
        {renderDefault()}
    </Dialog>

}
export default CommonDialog


export const CommonDrawer = (props) => {
    let {
        open,
        handleClose,
        title,
        customContent,
        customActions,
        leftButtonText,
        rightButtonText,
        leftButtonOnClick,
        rightButtonOnClick,
        leftButtonColor,
        leftButtonVariant,
        leftStartIcon,
        rightButtonColor,
        rightButtonVariant,
        rightStartIcon,
        rightButtonDisabled,
        handleSubmit,
        content,
        anchor,
        variant,
        customStyle,
        noTitle,
        customKey,
        id
    } = props


    const renderDefaultActions = () => {

        return <DialogActions
            sx={{
                ...customStyle?.actions
            }}
        >
            <Button
                id={customKey+"-left"}
                startIcon={leftStartIcon ? leftStartIcon : undefined}
                onClick={leftButtonOnClick ? leftButtonOnClick : handleClose}
                color={leftButtonColor || "secondary"}
                variant={leftButtonVariant || "text"}
            >
                {leftButtonText || ""}
            </Button>
            <Button
                id={customKey+"-right"}
                startIcon={rightStartIcon ? rightStartIcon : undefined}
                type="submit"
                disabled={rightButtonDisabled}
                variant={rightButtonVariant || "contained"}
                color={rightButtonColor || "primary"}
                onClick={rightButtonOnClick ? (e) => {
                    e.preventDefault()
                    rightButtonOnClick(e)
                } : undefined}
            >
                {rightButtonText || ""}
            </Button>
        </DialogActions>;
    };

    const renderDefaultContent = () => {

        return <form
            className={style.form}
            onSubmit={e => {
                e.preventDefault()
                if (!rightButtonOnClick) {
                    handleSubmit(e);
                }
            }}
        >
            <DialogContent
                sx={{
                    maxWidth: "30vw",
                    ...customStyle?.content
                }}
            >
                {content}
            </DialogContent>
            {customActions ?
                <DialogActions
                    sx={{
                        ...customStyle?.actions
                    }}
                >
                    {customActions}
                </DialogActions> :
                renderDefaultActions()
            }
        </form>
    };

    const renderDefault = () => {

        return <>
            {!noTitle && <DialogTitle
                sx={{
                    ...customStyle?.title
                }}
                id="draggable-dialog-title"
            >
                {title}
            </DialogTitle>}
            {customContent ?
                customContent
                : renderDefaultContent()
            }
            {customContent && customActions ?
                <DialogActions sx={{
                    ...customStyle?.actions
                }}>
                    {customActions}
                </DialogActions>
                : customContent &&
                renderDefaultActions()
            }</>
    }

    return <Drawer
        // key={key1}
        id={"mainContainerCommonDrawer"}
        anchor={anchor || "right"}
        variant={variant || "temporary"}
        open={open}
        sx={{
            // zIndex:130000
        }}
        onClose={handleClose}
        PaperProps={{id,sx: {width: "50%",borderRadius:0,...customStyle}}}

    >

        {renderDefault()}
    </Drawer>
}


export const useDialog = () => {
    let [props, setProps] = useState({
        open: false,
    })


    return [
        <CommonDialog
            {...props}
            open={props.open}
        />,
        (props
        ) => {
            setProps(props)
        },
        () => {
            setProps(p => ({...p, open: false}))
        },
        (isOpen) => {
            setProps({open: isOpen})
        },
        props
    ]
}
export const useDrawer = () => {
    let [props, setProps] = useState({
        open: false,
    })


    return [
        <CommonDrawer
            {...props}
            open={props.open}
        />,
        (props
        ) => {
            setProps(props)

        },
        () => {
            setProps(p => ({...p, open: false}))

        },
        (isOpen) => {
            setProps({open: isOpen})
        },
        props
    ]
}

