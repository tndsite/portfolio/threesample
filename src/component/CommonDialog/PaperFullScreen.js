import React, {Component} from 'react';
import Paper from '@mui/material/Paper';

class PaperFullScreen extends Component {
    render() {
        return (
                <Paper
                    {...this.props}
                    style={{
                        overflow: "hidden",
                        display: "flex",
                        flexDirection:"column",
                        justifyContent:"space-between",
                        flex:"1",
                        margin:0
                    }}
                />
        );
    }
}

export default PaperFullScreen;
