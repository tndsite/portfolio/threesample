import {Stack, Typography} from "@mui/material";

const LoadingPage = ({customSentence}) => {
    return <Stack  sx={{
        height: "100vh",
        width: "100vw",
        position: "absolute",
        backdropFilter: "blur(4px) brightness(0.5)",
        zIndex: 101000,
    }}>
        <Typography
            sx={{
                position: "absolute",
                top: "40%",
                left: "20px",
                right: "20px",
                marginLeft: "auto",
                marginRight: "auto",
            }}
            textAlign={"center"}
            variant={"title"}>{customSentence}</Typography>
    </Stack>
}

export default LoadingPage