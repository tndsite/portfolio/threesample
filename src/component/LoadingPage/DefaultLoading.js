import LoadingPage from "./LoadingPage";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";

const DefaultLoading = (props) => {
    let {store} = props
    let {loading} = store
    return loading.get && <LoadingPage/>
}

export default withCommonPropsLite(DefaultLoading)