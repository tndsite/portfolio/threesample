import {Drawer} from "@mui/material";
import React from "react";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";
import Sidebar from "../Sidebar";

const MenuDrawerHandler = (props) => {
    let {isMobile, store, intl, history} = props
    let {menuDrawerHandler, } = store
    let {open} = menuDrawerHandler.get

    return <Drawer

        anchor={isMobile ? "bottom" : "left"}
        variant={"persistent"}
        open={open}
        onClose={() => {
            menuDrawerHandler.set({open: false})
        }}
        onMouseEnter={()=>{
            menuDrawerHandler.set({open: true})
        }}
        onMouseLeave={()=>{

            // menuDrawerHandler.set({open: false})
        }}

        PaperProps={{
            sx: {
                width: isMobile?"100%":"180px",
                borderRadius: 0,
                height: isMobile?undefined:"calc(100% - 110px)",
                bottom: 0,
                marginTop: isMobile?0:"55px",
                marginBottom: isMobile?0:"55px",
                overflow:"hidden",
                border: "none"
            }
        }}

    >
        {<Sidebar forceShow={true} notRefreshApi={true}/>}
    </Drawer>

}
export default withCommonPropsLite(MenuDrawerHandler)
