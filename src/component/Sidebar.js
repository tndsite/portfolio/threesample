import {withCommonPropsLite} from "./interfacehook/InterfaceHook";
import {List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack} from "@mui/material";
import {Collections} from "@mui/icons-material";
import {HOME, SIGN_IN} from "../constants/Constants";
import {FormattedMessage} from "react-intl";
import React, {useEffect} from "react";
import ThemePicker from "./ThemePicker";
import LanguagePicker from "./LanguagePicker";
import Logo from "../asset/image/logo192.png";
import BottomMenu from "./BottomMenu";

const Sidebar = (props) => {
    let {
        store, isMobile, intl,
    } = props
    let {
        enabledView,
    } = store


    useEffect(() => {
        (async () => {

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return isMobile ?
        <BottomMenu/>
        : <Stack height={"100%"}
                 minWidth={isMobile ? "100%" : "190px"}
                 width={isMobile ? "100%" : "190px"}
                 sx={{
                     backgroundColor: "background.paper",
                     transition: "width 2s"
                 }}
                 justifyContent={"space-between"}
                 id={"sidebar-container"}>
            <Stack height={"100%"}
                   minWidth={isMobile ? "100%" : "190px"}
                   width={isMobile ? "100%" : "190px"}
                   sx={{
                       backgroundColor: "background.paper",
                       transition: "width 2s"
                   }}

                   id={"sidebar"}>
                <Stack direction={"column"} alignItems={"center"} justifyContent={"flex-end"}>
                    <List sx={{width: "100%", paddingTop: 0}}>
                        <ListItem key={"logo"} disablePadding sx={{padding: "0px 10px"}}>
                            <ListItemIcon>
                                <img src={Logo} style={{
                                    height: "40px",
                                    width: "40px"
                                }} alt={"logo-back-office"}/>
                            </ListItemIcon>
                            <ListItemText

                                primary={
                                    "Three sample"
                                }/>
                        </ListItem>
                        <ListItem key={"AdsManager"} disablePadding>
                            <ListItemButton

                                onClick={() => {
                                    enabledView.set(HOME)
                                }}

                                selected={enabledView.get === HOME}>
                                <ListItemIcon>
                                    <Collections/>
                                </ListItemIcon>
                                <ListItemText

                                    primary={
                                        intl?.formatMessage({id: "menu.adsManagement"})
                                    }/>
                            </ListItemButton>
                        </ListItem>
                    </List>
                </Stack>
            </Stack>
            <Stack alignItems={"center"} gap={"10px"} direction={isMobile ? "row" : "column"}>
                <ThemePicker/>
                <LanguagePicker/>
                <ListItemButton sx={{width: "100%"}}>
                    <ListItemText
                        sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                        primary={<FormattedMessage id={"logout"}/>}

                        onClick={() => {
                            enabledView.set(SIGN_IN)
                            sessionStorage.removeItem("auth_token")
                            window.location.href = window.location.origin
                        }}/>
                </ListItemButton>
            </Stack>

        </Stack>
}

export default withCommonPropsLite(Sidebar)
