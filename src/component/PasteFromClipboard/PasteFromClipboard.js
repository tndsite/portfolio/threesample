import {IconButton, InputAdornment, Stack, Tooltip} from "@mui/material";
import {FormattedMessage} from "react-intl";
import React from "react";
import CopyIcon from "../../asset/icons/CopyIcon";

const PasteFromClipboard = ({cb, id, disabled}) => {

    return <Stack direction={"row"}>
        {navigator?.clipboard?.readText ? <InputAdornment position="end">
            <Tooltip title={
                <FormattedMessage id={"pasteFromClipboard.tooltip"}/>
            }>
                <div>
                    <IconButton
                        disabled={disabled}
                        id={id + "PasteFromClipboardButton"}
                        onClick={async () => {
                            if (navigator?.clipboard?.readText) {
                                cb?.(await navigator.clipboard.readText())
                            }
                        }}
                    >
                        <CopyIcon secondary={"white"}/>
                    </IconButton>
                </div>
            </Tooltip>
        </InputAdornment> : ""}
    </Stack>
}

export default PasteFromClipboard