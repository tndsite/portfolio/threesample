import {uuidGenerator} from "../../constants/Utility";
import translationEN from "../../translation/translationEN.json"
import translationIT from "../../translation/translationIT.json"
import translationJP from "../../translation/translationJP.json"
import {createTheme} from "@mui/material";
import {SIGN_IN} from "../../constants/Constants";


export const createAppStore = () => {


    return {
        auth: {
            get: {},
            set(data) {
                this.get = data;
                if (data?.token) {
                    sessionStorage.setItem("auth_token", data.token);
                }
            }
        },
        ads: {
            get: [],
            set(data) {
                this.get = data;
            },
        },
        enabledView: {
            get: sessionStorage.getItem("enabledView") ?? SIGN_IN,
            set(data) {
                this.get = data;
                sessionStorage.setItem("enabledView", data)
            }
        },
        isDisconnected:
            {
                get: false,
                set(data) {
                    this.get = data;
                },
            },
        language: {
            get: localStorage.getItem("websiteLanguage") || "en",
            temp: localStorage.getItem("websiteLanguage") || "en",
            locales: [],
            list: {
                en: JSON.parse(localStorage.getItem("translations_en")) || translationEN,
                it: JSON.parse(localStorage.getItem("translations_it")) || translationIT,
                jp: JSON.parse(localStorage.getItem("translations_jp")) || translationJP,
            },
            set(data) {
                this.get = data
            },
            setLocales(data) {
                this.locales = data
            },
            setList(data) {

                for (let i in data) {
                    let locale = data[i]
                    for (let x in locale) {
                        if (locale[x] === "" || !locale[x]) {
                        } else {
                            this.list[i][x] = locale[x]
                        }
                    }

                    localStorage.setItem("translations_" + i, JSON.stringify(this.list[i]))


                }
            },
            setTemp(data) {
                this.temp = data
            }
        },
        theme: {
            get: createTheme(options),
            options: options,
            getDefaultTheme() {
                return createTheme(options)
            },
            set() {

                let palette, overrides
                switch (this.styleTmp) {
                    case "light":
                        palette = paletteLight
                        overrides = overridesLight
                        break
                    case "dark":
                    default:
                        palette = paletteDark
                        overrides = overridesDark
                        break
                }
                this.get = createTheme({
                    ...this.options,
                    components: {...this.options.components, ...overrides},
                    palette: palette
                })
            },
            style: localStorage.getItem("theme") || "light",
            setStyle(data) {
                this.style = data
            },
            styleTmp: localStorage.getItem("theme") || "light",
            setStyleTmp(data) {
                this.styleTmp = data
            }
        },
        appLoading: {
            get: true,
            set(data) {
                if (data !== this.get) {
                    this.get = data;
                }
            },
        },
        loading: {
            get: true,
            set(data) {
                if (data !== this.get) {
                    this.get = data;
                }
            },
        },
        localLoading: {
            get: true,
            set(data) {
                if (data !== this.get) {
                    this.get = data;
                }
            },
        },
        localLoadingLite: {
            get: false,
            set(data) {
                if (data !== this.get) {
                    this.get = data;
                }
            },
        },
        dialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },

        smallDialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        menuDrawerHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                },
                reset() {
                    this.get.open = false
                },
            },

        snackbarHandler:
            {
                get: {
                    open: false,
                    vertical: "top",
                    horizontal: "right",
                    autoHideDuration: null,
                    message: "",
                    type: "warning",
                    customColor: null,
                    customKey: null
                },
                success(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "success"
                    };
                },
                warning(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "warning"
                    };
                },
                error(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "error"
                    };
                }, info(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "info"
                    };
                },
                set(vertical = "top",
                    horizontal = "right",
                    open = false,
                    autoHideDuration = null,
                    message = "",
                    type = "warning",
                    customColor = null,
                    customKey = null
                ) {
                    this.get = {
                        vertical,
                        horizontal,
                        open,
                        autoHideDuration,
                        message,
                        type,
                        customColor,
                        customKey
                    };
                },
                reset() {
                    this.get.open = false
                },
            },


    }
}


const defaultTheme = createTheme();

export const paletteDark = {
    mode: 'dark',
    background: {
        default: "#2b2d31",
        mainSection: "#232428",
        torrentCard: "#1D1D1D",
        paper: "#1e1f22",
        paper2: "#292929",
        settingsItem: "#2A2A2A"
    },
    text: {
        primary: "#FFF",
        secondary: "#A1A1A1",
        darkContrast: "#000"
    },
    primary: {
        main: "#9EBAEE",
        contrastText: "#1D1D1D",
    },
    secondary: {
        main: "#99A1EC",
        contrastText: "#e0e4e9",
    },
    tertiary: {
        main: "#99A1EC",
        contrastText: "#1D1D1D",
    },
    success: {
        main: "#A6F1C9",
        contrastText: "#1D1D1D",
    },
    info: {
        main: "#A6C0F1",
        contrastText: "#fff",
    },
    warning: {
        main: "#FEEACC",
        contrastText: "#fff",
    },
    error: {
        main: "#FC7785",
        contrastText: "#fff",
    },
    button: {
        main: "#fffff1",
        hover: "#4a4a4b"
    },
    disabled: {
        main: "#707577",
        contrastText: "#a1a7ac"
    },
    border: {main: "#262626"}
}
export const paletteLight = {
    mode: 'light',
    background: {
        default: "#c1cee3",
        mainSection: "#fff",
        torrentCard: "#E5EEFF",
        paper: "#FFF",
        paper2: "#f8fafe",
        settingsItem: "#6C77DA",
    },
    text: {
        primary: "#010101",
        secondary: "#A1A1A1",
        contrast: "#fff",
        darkContrast: "#000"
    },
    primary: {
        main: "#6e82a6",
        contrastText: "#fff",
    },
    secondary: {
        main: "#fff",
        contrastText: "#012244",
    },
    tertiary: {
        main: "#99A1EC",
        contrastText: "#fff",
    },
    success: {
        main: "#A6F1C9",
        contrastText: "#fff",
    },
    info: {
        main: "#A6C0F1",
        contrastText: "#fff",
    },
    warning: {
        main: "#FEEACC",
        contrastText: "#fff",
    },
    button: {
        main: "#010101",
        hover: "#afafb0"
    },
    disabled: {
        main: "#707577",
        contrastText: "#012244"
    },

    border: {main: "#c7cdd2"}
}
export const overridesDark = {
    MuiTab: {
        root: {
            padding: "0px"
        }
    },

    MuiLinearProgress: {
        styleOverrides: {
            root: {
                borderRadius: "10px"
            }
        }
    },
    MuiListItemText: {
        styleOverrides: {
            root: {
                marginTop: "7px",
            },
            primary: {
                fontSize: "14px",
                fontWeight: 100,
            }
        }
    },

    MuiTableContainer: {
        styleOverrides: {
            root: {
                height: "100%",
            },
        },
    },

    MuiContainer: {
        styleOverrides: {
            root: {
                overflow: "hidden",
                paddingLeft: "0px",
                paddingRight: "0px",
                height: "100%",
                [defaultTheme.breakpoints.up('xs')]: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    paddingTop: "5px",
                }
            },
        },
    },
    MuiFilledInput: {
        styleOverrides: {
            root: {
                // backgroundColor: paletteDark.background.paper2,
                padding: 0,
                borderRadius: "16px",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },

            },
            input: {
                height: "70%!important",
                paddingLeft: "32px",
                paddingRight: "32px",
            },
            underline: {
                "&&&:before": {
                    borderBottom: "none"
                },
                "&&:after": {
                    borderBottom: "none"
                }
            }
        }
    },
    MuiDialogTitle: {
        styleOverrides: {
            root: {
                padding: "40px"
            },
        },
    },
    MuiDialogContent: {
        styleOverrides: {
            root: {
                minWidth: "100%",
                padding: "0 40px"
            },
        },
    },
    MuiDialogActions: {
        styleOverrides: {
            root: {
                padding: "40px",
                justifyContent: "center",
                gap: "8px"
            },
        },
    },
    MuiButton: {
        styleOverrides: {
            root: {
                textTransform: "unset",
                fontSize: "14px",
                fontWeight: 300,
                boxShadow: "none"
            },
        },
    },
    MuiPaper: {
        styleOverrides: {
            root: {}
        }
    },
    MuiTooltip: {
        styleOverrides: {
            tooltip: {
                background: "#161818",
                padding: "10px 10px",
                fontSize: "14px",
                fontWeight: 100
            },
        },
    }, MuiPopover: {
        styleOverrides: {
            paper: {
                backgroundColor: paletteDark.background.default + "!important",
            },
        },
    },
    MuiMenuItem: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    // backgroundColor: "transparent!important"
                },

            }
        },
    },
    MuiListItemButton: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    // backgroundColor: "transparent!important"
                },
                "&:hover": {
                    backgroundColor: "#555!important"
                }
            },

        }
    },

    MuiDivider: {
        styleOverrides: {
            root: {
                borderColor: "#F8FAFE"
            }
        }
    }
}
export const overridesLight = {

    MuiTab: {
        styleOverrides: {
            textColorPrimary: {
                // padding: "0px",
                color: "white!important"
            },

        }
    },
    MuiLinearProgress: {
        styleOverrides: {
            root: {
                borderRadius: "10px"
            }
        }
    },
    MuiListItemText: {
        styleOverrides: {
            root: {
                marginTop: "7px",
            },
            primary: {
                fontSize: "14px",
                fontWeight: 100,
            }
        }
    },

    MuiTableContainer: {
        styleOverrides: {
            root: {
                height: "100%",
            },
        },
    },

    MuiContainer: {
        styleOverrides: {
            root: {
                overflow: "hidden",
                paddingLeft: "0px",
                paddingRight: "0px",
                height: "100%",
                [defaultTheme.breakpoints.up('xs')]: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    paddingTop: "5px",
                }
            },
        },
    },
    MuiDialogTitle: {
        styleOverrides: {
            root: {
                padding: "40px"
            },
        },
    },
    MuiDialogContent: {
        styleOverrides: {
            root: {
                minWidth: "100%",
                padding: "0 40px"
            },
        },
    },
    MuiDialogActions: {
        styleOverrides: {
            root: {
                padding: "40px",
                justifyContent: "center",
                gap: "8px"
            },
        },
    },
    MuiButton: {
        styleOverrides: {
            root: {
                textTransform: "unset",
                fontSize: "14px",
                fontWeight: 300,
                boxShadow: "none"
            },
        },
    },
    MuiPaper: {
        styleOverrides: {
            root: {}
        }
    },

    MuiDivider: {
        styleOverrides: {
            root: {
                borderColor: "#292929"
            }
        }
    },

    MuiTooltip: {
        styleOverrides: {
            tooltip: {
                background: "#867979",
                padding: "10px 10px",
                fontSize: "14px",
                fontWeight: 100

            },
        },
    }, MuiPopover: {
        styleOverrides: {
            paper: {
                background: "#fff"
            },
        },
    },
    MuiMenuItem: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    backgroundColor: paletteLight.background.default + "!important"
                },
                "&:hover": {
                    backgroundColor: paletteLight.background.default + "!important"
                },
                backgroundColor: paletteLight.background.paper2 + "!important"
            }
        },
    },
    MuiListItemButton: {
        styleOverrides: {
            root: {
                // backgroundColor: paletteLight.background.paper2,
                "&.Mui-selected": {
                    // backgroundColor: paletteLight.background.default
                },
                "&:hover": {
                    backgroundColor: "#6e82a6!important",
                    color: "#fff"
                }
            },

        }
    },

    MuiFilledInput: {
        styleOverrides: {
            root: {
                backgroundColor: paletteLight.background.mainSection,
                padding: 0,
                borderRadius: "16px",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },

            },
            input: {
                height: "70%!important",
                paddingLeft: "32px",
                paddingRight: "32px",
            },
            underline: {
                "&&&:before": {
                    borderBottom: "none"
                },
                "&&:after": {
                    borderBottom: "none"
                }
            }
        }
    },
}


const options = {
    typography: {
        fontFamily: "Euclid Circular A",
        fontSize: 12,
        menuLabel: {
            fontStyle: "normal",
            fontWeight: 600,
            fontSize: "12px",
            lineHeight: "140%",
            textAlign: "center",
            textTransform: "uppercase",
        },
        title30: {
            fontSize: "30px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        title30Black: {
            fontSize: "30px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px",
            color: "black"
        },
        title: {
            fontSize: "26px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        title24: {
            fontSize: "24px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        cardTitle: {
            fontSize: "16px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "1px"
        },
        semiTitle: {

            fontSize: "20px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        }, semiTitleBlack: {
            color: "black",
            fontSize: "20px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        subTitle18: {

            fontSize: "18px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        subTitle: {

            fontSize: "17px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        smallText16: {

            fontSize: "16px",
            fontWeight: 200,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        }, smallText16Black: {

            fontSize: "16px",
            fontWeight: 200,
            lineHeight: "150%",
            letterSpacing: "0.4px",
            color: "black"
        }, smallText: {

            fontSize: "15px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        smallText14: {

            fontSize: "14px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        miniText:
            {

                fontSize: "13px",
                fontWeight: 500,
                lineHeight: "150%",
                letterSpacing: "0.4px"
            }
        ,
        microText: {
            color: "#9f9f9f",
            fontSize: "12px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        microTextW: {
            fontSize: "12px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        microText10: {
            fontSize: "10px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        noData: {

            fontSize: "30px",
            fontWeight: 500,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        upperCase14: {

            fontSize: "14px",
            fontWeight: "500",
            textAlign: "center",
            textTransform: "uppercase",
            letterSpacing: "1.5px"
        }
    },
    palette: ((localStorage.getItem("theme") && localStorage.getItem("theme") === "dark")) ? paletteDark : paletteLight,
    components: {

        ...((localStorage.getItem("theme") && localStorage.getItem("theme") === "dark")) ? overridesDark : overridesLight,
    },
};
