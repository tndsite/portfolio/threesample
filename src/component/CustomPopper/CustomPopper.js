import Fade from "@mui/material/Fade";
import Paper from "@mui/material/Paper";
import React from "react";
import Popper from "@mui/material/Popper";
import {ClickAwayListener} from "@mui/material";

const CustomPopper = ({sx={},sxPaper={},id, open, anchorEl, handleClickAway, children, placement = "bottom-start"}) => {


    return <Popper
        open={open}
        anchorEl={anchorEl}
         id={id}
        sx={{
            // zIndex:130001,
            ...sx
        }}
        transition
        placement={placement}>
        {({TransitionProps}) => (
            <ClickAwayListener
                onClickAway={handleClickAway}
            >
                <Fade {...TransitionProps} timeout={350}>
                    <Paper sx={{
                        background: "transparent",
                        backgroundColor: "background.paper",
                        borderRadius:"4px!important",...sxPaper}}>
                        {children}
                    </Paper>
                </Fade>
            </ClickAwayListener>
        )}
    </Popper>
}

export default CustomPopper