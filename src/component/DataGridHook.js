import * as React from 'react';
import {
    DataGrid,
    enUS,
    GridToolbar,
    GridToolbarColumnsButton,
    GridToolbarContainer,
    GridToolbarDensitySelector,
    GridToolbarFilterButton,
    itIT,
    trTR,
    useGridApiRef
} from '@mui/x-data-grid';
import {Box} from "@mui/material";
import {getFillWidth} from "../constants/Utility";

function CustomToolbar(props) {
    return (
        <GridToolbarContainer>
            <GridToolbarColumnsButton/>
            <GridToolbarFilterButton/>
            <GridToolbarDensitySelector/>

        </GridToolbarContainer>
    );
}


export default function DataGridHook(props) {
    let apiRef = useGridApiRef();
    const [, forceUpdate] = React.useReducer(x => x + 1, 0);
    let {
        callback,
        rows,
        columns,
        onRowEditStart,
        loading,
        dataGridName,
        enableCustomComponents,
        hideHeader,
        isCellEditable = () => true,
        setSelected,
        selectionModel,
        checkboxSelection = true,
        isEditOn,
        translationKey
    } = props
    const [density, setDensity] = React.useState(JSON.parse(localStorage.getItem(dataGridName + "Density")) || 'standard');
    const handleRowEditCommit = React.useCallback(
        async (newRow) => {
            await callback(newRow)
        },
        [apiRef, rows],
    );

    let sortModel = JSON.parse(localStorage.getItem(dataGridName + "Sorters")),
        filterModel = JSON.parse(localStorage.getItem(dataGridName + "Filters"))

    let filtersSorters
    if (sortModel && !filterModel) {
        filtersSorters = {sortModel}
    } else if (filterModel && !sortModel) {
        filtersSorters = {filterModel}
    } else if (filterModel && sortModel) {
        filtersSorters = {filterModel, sortModel}

    }

    let localization = enUS
    switch (localStorage.getItem("websiteLanguageCustom")) {
        case "it":
            localization = itIT
            break
        case "tr":
            localization = trTR
            break
        case "sv":
        case "en":
        default:
            localization = enUS
            break
    }

    let customComponents
    if (enableCustomComponents) {

        customComponents = {
            Toolbar: () => <CustomToolbar
                isEditOn={isEditOn}
                translationKey={translationKey}
            />,
        }
    }

    let headerProps = {}
    if (hideHeader) {
        // headerProps = {headerHeight: 0}
    }
    return (
        <Box
            sx={{
                height: getFillWidth(),
                // minHeight: "400px",
                padding: "10px",
                width: "100%",
                '& .MuiDataGrid-cell--editing': {
                    bgcolor: 'rgb(255,215,115, 0.19)',
                    color: '#1a3e72',
                },
                '& .Mui-error': {
                    bgcolor: (theme) =>
                        `rgb(126,10,15, ${theme.palette.mode === 'dark' ? 0 : 0.1})`,
                    color: (theme) => (theme.palette.mode === 'dark' ? '#ff4343' : '#750f0f'),
                },
            }}
        >

            <DataGrid
                density={density}
                disableSelectionOnClick={true}
                checkboxSelection={checkboxSelection}
                isCellEditable={isCellEditable}
                pageSize={100}
                {...headerProps}
                onSelectionModelChange={(newSelectionModel) => {
                    setSelected(newSelectionModel)
                }}
                selectionModel={selectionModel}
                onStateChange={v => {
                    localStorage.setItem(dataGridName + "Density", JSON.stringify(v.density.value))
                    density !== v.density.value && setDensity(v.density.value)
                }}
                onFilterModelChange={(filters) => {
                    localStorage.setItem(dataGridName + "Filters", JSON.stringify(filters))
                    forceUpdate()
                }}
                onSortModelChange={(sorters) => {
                    localStorage.setItem(dataGridName + "Sorters", JSON.stringify(sorters))
                    forceUpdate()
                }}
                {...filtersSorters}
                onColumnVisibilityChange={(column) => {
                    let tmpColumns = []
                    columns.forEach(x => {
                        if (x.field === column.field) {
                            tmpColumns.push({...x, hide: !column.isVisible})
                        } else {
                            tmpColumns.push(x)
                        }
                    })
                    localStorage.setItem(dataGridName, JSON.stringify(tmpColumns))
                }}
                localeText={localization.components.MuiDataGrid.defaultProps.localeText}
                components={{...customComponents}}
                onCellClick={(params, event) => {
                    if (!props.isEditOn) {
                        let doubleClickEvent = document.createEvent('MouseEvents');
                        doubleClickEvent.initEvent('dblclick', true, true);
                        event.currentTarget.dispatchEvent(doubleClickEvent);
                    }
                }}
                onCellDoubleClick={() => {
                }}
                slots={{toolbar: GridToolbar}}
                slotProps={{
                    toolbar: {
                        showQuickFilter: true,
                    },
                }}

                apiRef={apiRef}
                rows={rows}
                columns={columns}
                loading={loading}
                editMode="row"
                onRowEditStart={onRowEditStart}
                onRowEditStop={async (newRow) => {
                    await handleRowEditCommit(newRow)
                }}
                componentsProps={{}}
            />
        </Box>
    );
}
