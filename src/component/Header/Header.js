import React, {useEffect, useState} from "react";
import {Stack} from "@mui/material";
import packageJson from "../../../package.json"
import style from "./Header.module.css"
import {withCommonProps} from "../interfacehook/InterfaceHook";

let version = packageJson.version
const Header = (props) => {
    // let [state, setState] = useState({})
    // let {} = state
    let {customElement, history, isDesktop, isMobile, containerScroller, store} = props
    let [lastScrollTop, setLastScrollTop] = useState(0);
    let {dialogHandler, changelogDialogHandler, checkBadge, sidebarExpanded, menuDrawerHandler} = store
    let searchTimeout

    let [onHover, sH] = useState(false)
    useEffect(() => {
        (async () => {
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {


        const doSomething = (e) => {
            // Do something with the scroll position
            let header = document.getElementById("hidableHeader")
            let container = e.target
            let scrollTop = container.scrollTop;
            if (scrollTop > lastScrollTop) {
                header.style.top = "-" + header.offsetHeight + "px"
            } else {
                header.style.top = "0"
            }
            setLastScrollTop(scrollTop)
        }


        const wrap = (e) => {
            clearTimeout(searchTimeout);
            // eslint-disable-next-line react-hooks/exhaustive-deps
            searchTimeout = setTimeout(() => {
                doSomething(e);
            }, 200)
        }

        if (containerScroller && containerScroller.current && containerScroller.current.getAttribute('listener') !== 'true') {
            containerScroller.current.addEventListener("scroll", wrap)
            containerScroller.current.setAttribute('listener', 'true');
        }

        return () => {
            if (containerScroller && containerScroller.current && containerScroller.current.getAttribute('listener') === 'true') {
                containerScroller.current.removeEventListener("scroll", wrap)
                // eslint-disable-next-line react-hooks/exhaustive-deps
                containerScroller.current.setAttribute('listener', 'false');
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [lastScrollTop])


    let enviroment = ""
    if (process.env.REACT_APP_BETA === "true") {
        enviroment = "-beta"
    } else if (process.env.REACT_APP_ALPHA === "true") {
        enviroment = "-alpha"
    }

    return (<Stack className={style.header} id={"hidableHeader"}
                   sx={{
                       height: !customElement(isDesktop) ? "115px" : "170px",
                       left:  "190px",
                       backgroundColor: "background.default",
                       right: "6px"
                   }}
        >
            {dialogHandler.get.open ? null : customElement(isDesktop) && <Stack
                justifyContent={"space-between"}
                alignItems={"center"}
                direction={"row"} width={"100%"} height={"100%"}>
                <Stack direction={"row"} alignItems={"center"}
                       justifyContent={"flex-start"}
                       paddingLeft={isMobile ? "10px" : undefined}
                       sx={{

                           zIndex: 100,
                       }}>
                </Stack>
                <Stack sx={{
                    height: "100%"
                }}
                       alignItems={"center"}
                       justifyContent={"center"}
                >
                    {customElement(isDesktop)}
                </Stack>
                <Stack direction={"row"} alignItems={"center"} sx={{
                    zIndex: 100,
                }}>
                </Stack>
            </Stack>}
        </Stack>
    );


}


export default withCommonProps(Header)
