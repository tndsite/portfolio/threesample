import {Button, Stack, Tooltip} from "@mui/material";
import React from "react";
import {Collections, Logout} from "@mui/icons-material";
import {withCommonPropsLite} from "./interfacehook/InterfaceHook";
import {HOME, SIGN_IN} from "../constants/Constants";
import ThemePicker from "./ThemePicker";
import LanguagePicker from "./LanguagePicker";


BottomMenu.defaultProps = {}

function BottomMenu(props) {
    let {intl, store,} = props;
    let {enabledView,} = store


    return <Stack
        id={"menu-main-container"}
        width={"100%"}
        height={"80px"}
        justifyContent={"space-around"}
        alignItems={"center"}
        direction={"row"}
        sx={{position: "fixed", bottom: 0, backgroundColor: "background.paper", zIndex: 13005}}
    >
        {
            [{
                id: "menu-button-home",
                selected: enabledView.get === HOME,
                onClick: () => {
                    enabledView.set(HOME)
                },
                icon: (selected) => <Collections/>,
                label: intl.formatMessage({id: "menu.adsManagement"},)
            },
            ].map(x => {


                const wrap = (key) => {

                    return <Button
                        disabled={x.disabled}
                        key={key}
                        id={x.id}
                        selected={x.selected}
                        onClick={x.onClick}
                        sx={{
                            height: "54px",
                            width: "54px",
                            padding: 0
                        }}
                    >
                        {x.icon(x.selected)}
                    </Button>
                }

                return <Tooltip title={x.label} key={x.id}>{wrap()}</Tooltip>

            })
        }
        <ThemePicker/>
        <LanguagePicker/>
        <Button
            sx={{
                height: "54px",
                width: "54px",
                padding: 0
            }}
            onClick={() => {
                enabledView.set(SIGN_IN)
                sessionStorage.removeItem("auth_token")
                window.location.href = window.location.origin
            }}
        >
            <Logout/>
        </Button>
    </Stack>
}

export default withCommonPropsLite(BottomMenu)
