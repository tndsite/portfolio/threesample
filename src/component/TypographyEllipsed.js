import {ClickAwayListener, Tooltip, Typography} from "@mui/material";
import React, {useMemo, useState} from "react";
import {uuidGenerator} from "../constants/Utility";


export const TypographyEllipsed = ({children, variant, fontWeight, sx, sxContainer = {}}) => {
    let [open, setOpen] = useState(false)
    let id = useMemo(() => {
        return "typography" + uuidGenerator()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (!children) {
        return null
    }


    return <ClickAwayListener onClickAway={() => setOpen(false)}>
        <Tooltip
            PopperProps={{
                sx: {
                    "& .MuiTooltip-tooltip": {
                        backgroundColor: 'black'
                    },
                },
            }}
            open={open}
            title={children}>
            <Typography component={"div"}
                        onClick={() => setOpen(!open)}
                        variant={variant}
                        fontWeight={fontWeight || 100}
                        sx={{
                            width: "100%",
                            // fontSize: "15px",
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            margin: "0",
                            display:"block",
                            overflowWrap: "anywhere",
                            ...sx
                        }}>
                {children}
            </Typography>
        </Tooltip>
    </ClickAwayListener>
}