import {copyToClipboard} from "../../constants/Utility";
import {IconButton, Tooltip} from "@mui/material";
import {FormattedMessage} from "react-intl";
import {ContentCopy} from "@mui/icons-material";

const CopyToClipboard=({text,hideInSnackBar,hideSnackbar})=>{

    return navigator?.clipboard?.readText ?
            <Tooltip title={
                <FormattedMessage id={"copyToClipboard.tooltip"} />
            }>
                <IconButton
                    onClick={async () => {
                        await copyToClipboard(text,hideInSnackBar,hideSnackbar)

                    }}
                >
                    <ContentCopy fontSize={"small"}/>
                </IconButton>
            </Tooltip>
        : ""
}

export default CopyToClipboard