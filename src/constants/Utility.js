import BaseAxios from "./BaseAxios";
import {FormattedMessage} from "react-intl";
import {APP_LOADING, LOADING, LOCAL_LOADING, LOCAL_LOADING_LITE, NO_LOADING} from "./Constants";

export const uuidGenerator = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        // eslint-disable-next-line no-mixed-operators
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}



export const copyToClipboard = async (text, hideInSnackBar, hideSnackbar) => {
    let store = window.TNDglobalStoreMobx
    let {snackbarHandler} = store
    if (getDevice()) {
        let dummy = document.createElement("textarea");
        document.body.appendChild(dummy);
        dummy.value = text;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    } else {
        await navigator.clipboard.writeText(text)
    }
    if (!hideSnackbar) {
        if (hideInSnackBar) {
            snackbarHandler.success(<FormattedMessage id={"copiedToClipboard.hideInSnackBar"}/>)
        } else {
            snackbarHandler.success(<FormattedMessage id={"copiedToClipboard"} values={{text}}/>)
        }
    }


}

export const getDevice = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)
}

export const getBase64FromUrl = async (src, fileName) => {
    const response = await BaseAxios.getTND(src, {
        responseType: "blob",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS"
    });

    return response.data
};



export const commonErrorHandler = (props) => {
    let {errorText} = props;
    let store = window.TNDglobalStoreMobx
    // let intl = window.TNDintl
    let {snackbarHandler} = store


    // if (tmpEr) {
    //     errorText = intl.formatMessage({id: "error.translation.code." + tmpEr})
    // }

    snackbarHandler.warning(errorText);

}


export const commonHandledRequest = async (props) => {
    let {
        translationKeyError, action, loadingType, translationKeySuccess, delayLoadingEnd = 0, snackbarType,
        cbOnError
    } = props
    let store = window.TNDglobalStoreMobx
    let {snackbarHandler, localLoading, localLoadingLite, appLoading,loading:defLoading} = store
    let loading
    switch (loadingType) {
        case NO_LOADING:
            loading = {
                set: () => {
                }
            }
            break
        case LOADING:
            loading = defLoading
            break
        case LOCAL_LOADING:
            loading = localLoading
            break
        case APP_LOADING:
            loading = appLoading
            break
        case LOCAL_LOADING_LITE:
        default:
            loading = localLoadingLite
            break
    }
    try {
        loading.set(true)
        await action()
        setTimeout(() => {
            loading.set(false)
            if (translationKeySuccess) {
                let txt = <FormattedMessage id={translationKeySuccess + ".snackbar.success"}/>
                switch (snackbarType) {
                    case "success":
                    default:
                        snackbarHandler.success(txt)
                        break
                    case "info":
                        snackbarHandler.info(txt)
                        break

                }
            }
        }, delayLoadingEnd)

    } catch (error) {
        loading.set(false)
        console.error("Error commonHandledRequest", error, error.response)
        if (cbOnError) {
            cbOnError?.(error)
        } else {
            commonErrorHandler({error, errorText: <FormattedMessage id={translationKeyError + ".snackbar.error"}/>})
        }
    }
}


export const getBrowserInfo = () => {
    let ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name: 'IE ', version: (tem[1] || '')};
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return {name: 'Opera', version: tem[1]};
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return {
        name: M[0] || "",
        version: M[1]
    };
}


export const getFillWidth = () => {
    try {
        if (getBrowserInfo().name.toLowerCase().includes("firefox")) {
            return "-moz-available";
        } else {
            return "-webkit-fill-available";
        }
    } catch (e) {
        console.error("Failed to identify the browser: ", e);
        return "-webkit-fill-available";
    }
}