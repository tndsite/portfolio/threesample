import axios from "axios";
import {BACKEND_PATH, IS_STAGING} from "./Constants";


const BaseAxios = axios.create({
    baseURL: BACKEND_PATH
});

const API_KEY_TRANSLATIONS_TOOL = IS_STAGING ?
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImZlZDFkNGUwLTJlYjktNDc1My05NGUxLTkyZjVlY2MyMzRmNyIsInByb2R1Y3RJZCI6IjIyNTUyMWM5LTBjNDktNDczYy1iOWIxLWQ1ODBkNzU0MjBlMiIsInNlcnZpY2VJZCI6NiwiaWF0IjoxNzA5OTkyMTE5LCJleHAiOjE3NDE1MjgxMTl9.gc7HAO5WX9BFcZ65RSvyiNxsLIG2BYroBV9Vwf-lZzk"
    :
    "";

const getApiKeyByPath = (path) => {
    if (path?.includes("product")) {
        return sessionStorage.getItem("auth_token");
    } else if (path?.includes("service/translations-tool/")) {
        return API_KEY_TRANSLATIONS_TOOL
    }
}


// 2. Log della richiesta nell api
BaseAxios.interceptors.request.use(async request => {


    request.headers["Access-Control-Allow-Origin"] = "*";
    request.headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept";
    request.headers["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,PATCH,OPTIONS";

    if ((request.headers["Authorization"] === undefined || request.headers["Authorization"] === null || request.headers["Authorization"] === "")) {
        request.headers["Authorization"] = getApiKeyByPath(request.url);
    }
    request.meta = request.meta || {}
    request.meta.requestStartedAt = new Date().getTime();
    request.meta.logData = {
        startLog: ("REQUEST " + request.method.toUpperCase() + " " + request.url),
        startData: request.data ? {data: request.data, request} : request
    }
    return request;
});

const consoleHandler = (response) => {
    console.groupCollapsed(response.config.meta.logData.startLog + `Time: ${new Date().getTime() - response.config.meta.requestStartedAt} ms`)
    if (new Date().getTime() - response.config.meta.requestStartedAt > 1500) {
        console.error("LONG TIME RESPONSE: Time: " + new Date().getTime() - response.config.meta.requestStartedAt + " ms")
    }
    console.debug("Starting config:", response.config.meta.logData.startData);
    console.debug("Response:", response.data)
    let columns = {}
    for (let i in response.data) {
        if (response.data[i]
        ) {
            if (
                typeof response.data[i] !== 'object'
                &&
                !Array.isArray(response.data[i])
            ) {
                columns[i] = response.data[i]
            } else {
                for (let oIndex in response.data[i]) {
                    if (typeof response.data[i][oIndex] !== 'object'
                        &&
                        !Array.isArray(response.data[i][oIndex])) {
                        if (response.data[i][oIndex]) {
                            if (!columns[i]) {
                                columns[i] = {
                                    [oIndex]: response.data[i][oIndex]
                                }
                            } else {
                                columns[i] = {
                                    ...columns[i],
                                    [oIndex]: response.data[i][oIndex]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (Object.keys(columns).length > 0) {
        console.table(columns);
    }
    console.groupEnd();
}

// 3. Log della risposta dall api
BaseAxios.interceptors.response.use(
    async response => {
        if(response.headers["Authorization"]) {
            sessionStorage.setItem("auth_token", response.headers["Authorization"])
        }
        consoleHandler(response)

        return response;
    },
    error => {
        if (error.response) {
            if (
                error.response.status === 401
                // ||
                // error.response.status === 400
            ) {
                sessionStorage.removeItem("auth_token")
                window.location.href = window.location.origin
            }

            console.groupCollapsed("%c ERROR Status: " + (error.response && error.response.status) + " Code: " + error.code + " " + (error.response && error.response.config.meta.logData.startLog) + `Time: ${new Date().getTime() - (error.response && error.response.config.meta.requestStartedAt)} ms`, 'border:2px solid red;background: #5c0000; color: #ff8080')
            console.table({...error.response.data, status: error.response.status, code: error.code})
            console.error("ERROR RESPONSE", error.response);
            console.error("ERROR", error,);
            console.groupEnd();
        } else if (error.message !== "FEE001") {
            console.error("ERROR", error)
        }
        return Promise.reject(error);
    }
);


export default BaseAxios


